<?php
/*
Plugin Name:  GO Salesforce Donation Information
Version: 0.1
Plugin URI:  http://www.groundworkopportunities.org/
Description:  Gets information about a Campaign from Salesforce.com
Author: Michael Klein
Author URI:  mailto:mike@groundworkopportunites.org
License:
 Released under the GPL license
  http://www.gnu.org/copyleft/gpl.html
  Copyright 2012 Groundwork Opportunities (email : mike@groundworkopportunites.org)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

/*-----------------------------------------------------------------------------------*/
/* Load REST Helper class
/*-----------------------------------------------------------------------------------*/
if (!class_exists('GO_Salesforce_RestHelper')) {
	require_once(dirname(__FILE__).'/classes/class_go_salesforce_resthelper.php');
}

global $salesforce_resthelper;

if ($salesforce_resthelper == NULL) {
	$salesforce_resthelper = new GO_Salesforce_RestHelper();
}
/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/* Load Campaign Info class
/*-----------------------------------------------------------------------------------*/
if (!class_exists('GO_Salesforce_Campaign_Info')) {
	require_once(dirname(__FILE__).'/classes/class_go_salesforce_campaign_info.php');
}

global $salesforce_campaign_info;

if ($salesforce_campaign_info == NULL) {
	$salesforce_campaign_info = new GO_Salesforce_Campaign_Info();
}
/*-----------------------------------------------------------------------------------*/

/**
 * Provides the value of a field from a Salesforce.com campaign
 *
 * @access public
 * @return Value of the salesforce field
 */
function go_salesforce_campaign_field($campaign_id, $field_name) {
	global $salesforce_campaign_info;
	
	return $salesforce_campaign_info->campaign_field($campaign_id, $field_name);
}

/**
 * Provides the value of a field from a Salesforce.com campaign as a shortcode
 *
 * @access public
 * @return Value of the salesforce field
 */
function go_salesforce_campaign_field_shortcode($atts) {
	extract( shortcode_atts( array(
		'campaign_id' => null,
		'field_name' => null
	), $atts ) );
	
	$rtnVal = null;
	if($campaign_id != null and $field_name != null) {
		$rtnVal = go_salesforce_campaign_field($campaign_id, $field_name);
	}
	return $rtnVal;
}
add_shortcode('campaign-field', 'go_salesforce_campaign_field_shortcode');


/**
 * Provides a formatted funding status bar from Salesforce.com campaign as a shortcode
 *
 * @access public
 * @return HTML formatted progress bar
 */
function go_salesforce_progress_bar_shortcode($atts) {
	extract( shortcode_atts( array(
		'campaign_id' => null,
		'field_list' => null
	), $atts ) );

	$rtnVal = null;
	
	if($field_list == null) {
		//If field list is not provided, use default
		$fieldArr = explode(",", "expectedRev,fundingToSend,timeLeft");
	}
	else {
		$fieldArr = explode(",", $field_list);
	}

	if($campaign_id != null) {
		$fieldHtml = null;

		foreach($fieldArr as $field) {
			$fieldHtml = $fieldHtml . sprintf(
								'<div id="fundraising-goal" class="item-header-box">'
									.'<span><strong>%s</strong></span>'
									.'<span class="small">%s</span>'
								.'</div>'
								,go_salesforce_campaign_field($campaign_id, $field)
								,go_salesforce_get_pretty_field_name($field)
						);
		}
		
		$rtnVal = sprintf('<div class="champion-stats">'
							.'<div class="progress-bar">'
								.'<div class="progress-bar-loader" style="width:%s%%"></div>'
							.'</div>'
							.'%s'
						.'</div>'
						, go_salesforce_campaign_field($campaign_id, "fundingToSendProgress")
						, $fieldHtml
				);
	}
	return $rtnVal;
}
add_shortcode('campaign-progress-bar', 'go_salesforce_progress_bar_shortcode');



/**
 * Provides the "pretty" name for campaign status fields
 *
 * @access public
 * @return "pretty" field name as a string
 */
function go_salesforce_get_pretty_field_name($fieldShortName) {
	
	$rtnVal = $fieldShortName;
	
	if($fieldShortName == "expectedRev") {
		$rtnVal = "goal";
	} elseif ($fieldShortName == "fundingToSend") {
		$rtnVal = "raised";
	} elseif ($fieldShortName == "timeLeft") {
		$rtnVal = "days left";
	} elseif ($fieldShortName == "amountWon") {
		$rtnVal = "raised";
	} elseif ($fieldShortName == "dollarsToGo") {
		$rtnVal = "dollars to go";
	} elseif ($fieldShortName == "backers") {
		$rtnVal = "backers";
	} elseif ($fieldShortName == "numDonationWon") {
		$rtnVal = "donations";
	} 

	return $rtnVal;
}
?>
