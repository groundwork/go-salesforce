<?php
/**
 * GO Salesforce Campaign Information
 *
 * The Campaign Information class provides details about a campaign from Salesforce.com 
 * Configuration of this class is performed via the GO Salesforce Admin Menus plugin
 *
 * @class 		GO_Salesforce_Campaign_Info
 * @version		0.2
 * @package		GO Salesforce/Classes
 * @author 		Groundwork Opportunities
 */
class GO_Salesforce_Campaign_Info {

	private $_rtnObj;
	private $_campaign_id;

	/**
	 * Constructor for the Campaign Info class. Sets all values to defaults.
	 *
	 * @access public
	 * @return void
	 */
	function __construct() {
		$this->init_vals();
	}
	
	/**
	 * Sets up default values
	 *
	 * @access private
	 * @return void
	 */
	private function  init_vals() {
		$this->_rtnObj = array(
				'progress' => '0'
				,'fundingToSendProgress' => '0'
				,'amountWon' => '--' 
				,'backers' => '--'
				,'expectedRev' => '0'
				,'timeLeft' => '--'
				,'goal' => '0'
				,'numDonationWon' => '--'
				,'dollarsToGo' => '--'
			);
	}
	
	/**
	 * Gets the value of a campaign field
	 *
	 * @access public
	 * @return field value
	 */
	public function campaign_field($campaign_id, $field_name) {
		// if the data is not already loaded in memory
		if($campaign_id != $this->_campaign_id or $this->_rtnObj == null) {
			// if APC caching is enabled
			if (function_exists("apc_fetch")) {
				$obj = apc_fetch($campaign_id);
			} else {
				$obj = FALSE;
			}
			// and the object is not in the cache
			if (FALSE === $obj) {
				// then fetch the data remotely
				$this->load_campaign_info($campaign_id);
			} else {
				// otherwise use the cached data
				$this->_rtnObj = $obj;
			}
		}
		return $this->_rtnObj[$field_name];
	}


	/**
	 * Load campaign information from Salesforce.com
	 *
	 * @access private
	 * @return none
	 */
	private function load_campaign_info($campaign_id) {
		global $bp;  //Use BuddyPress
		global $salesforce_resthelper;  //Use REST Helper
		
		$this->_campaign_id = $campaign_id;
		
		//Get REST detaiils from admin configuration
		$restlet_uri = get_option('go_salesforce_campaign_progress_restlet_uri');
		$restlet_action = get_option('go_salesforce_campaign_progress_action');
		
		// Default to 3 hours. Cache is invalidated from go-salesforce-checkout.php
		// whenever a user donates to a campaign. This should ensure immediate fresh data.
		$cache_timeout = 3 * 60 * 60;

		try {
		
			//rest_helper function is defined in the theme functions.php file
			//Makes webservice call to Salesforce.com
			$this->_rtnObj = get_object_vars(
				$salesforce_resthelper->send_request(
					$restlet_uri
					,array(
						'a' => $restlet_action
						,'cam_name' => $campaign_id
					),'POST'
					,'json'
				)
			);

			// Calculated values
			$this->_rtnObj['fundingToSendProgress'] = ( $this->_rtnObj['fundingToSend'] / $this->_rtnObj['expectedRev'] ) * 100;

			// Add the data to the APC cache
			if (function_exists("apc_store")) {
				apc_store($campaign_id, $this->_rtnObj, $cache_timeout);
			}
		
			// The following are the variables available for use from SalesForce
			//$progress = $rtnObj['progress'];
			//$amountWon = $rtnObj['amountWon'];
			//$backers = $rtnObj['backers'];
			//$expectedRev = $rtnObj['expectedRev'];
			//$timeLeft = $rtnObj['timeLeft'];
			//$goal = $rtnObj['goal'];
			
		} catch (Exception $e) {
			//An error occurred in the webservice call
			//echo "<pre>";
			//echo var_dump($e);
			//echo "</pre>";
		}
	}

}
?>